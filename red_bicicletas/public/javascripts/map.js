var map = L.map('main_map').setView([10.9838942,-74.853037], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
/*
L.marker([11.0131395,-74.8276716]).addTo(map);
L.marker([11.0066653,-74.8200118]).addTo(map);
L.marker([11.0082898,-74.8218244]).addTo(map);*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})